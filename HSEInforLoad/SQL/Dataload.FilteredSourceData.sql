
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[FilteredSourceData]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[FilteredSourceData](
	GradeId NVARCHAR(50),
	GenderId INT,
	AgencyId INT,
	YearId INT,
	MonthId INT,
	EmpTypeId INT,
	HeadCount INT,
	Wte FLOAT
) ON [PRIMARY]
END
GO

--DROP TABLE [Dataload].[FilteredSourceData]

