USE HSE_Numbers

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Dataload')
BEGIN
	EXEC('CREATE SCHEMA Dataload')
END