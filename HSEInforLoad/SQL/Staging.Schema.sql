USE HSE_Numbers

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Staging')
BEGIN
	EXEC('CREATE SCHEMA Staging')
END