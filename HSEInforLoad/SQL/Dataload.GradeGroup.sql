
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[GrCat]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[GrCat](
	[GrCat] INT NULL,
	[GrCatDesc] NVARCHAR(MAX) NULL,
	[Obsolete] VARCHAR(10) NULL,
	[InforMemId] [int] NULL
) ON [PRIMARY] 

END
GO

--DROP TABLE [Dataload].[GrCat]
SELECT * FROM [Dataload].[GrCat]

IF NOT EXISTS (SELECT 1 FROM [Dataload].[GrCat])
BEGIN 
	INSERT INTO [Dataload].[GrCat]([GrCat], [GrCatDesc], [Obsolete], [InforMemId])
	SELECT 1, 'Management/ Admin', 'N', 84000001 UNION ALL
	SELECT 2, 'Medical/ Dental', 'N', 84000002 UNION ALL
	SELECT 3, 'Nursing', 'N', 84000003 UNION ALL
	SELECT 4, 'Health & Social Care Professionals', 'N', 84000004 UNION ALL
	SELECT 5, 'General Support', 'N', 84000005 UNION ALL
	SELECT 7, 'Patient & Client Care', 'N', 84000006
END


