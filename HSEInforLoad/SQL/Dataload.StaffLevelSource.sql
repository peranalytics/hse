
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[StafflevelSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[StafflevelSource](
	AgencyCode INT,
	GradeCode NVARCHAR(50),
	YearId INT,
	MonthId INT,
	GenderId INT,
	EmpTypeId INT,
	PerFtNumMale INT,
	PerFtNumFemale INT,
	PerFtWteMale FLOAT,
	PerFtWteFemale FLOAT,
	PerPtNumMale INT,
	PerPtNumFemale INT,
	PerPtWteMale FLOAT,
	PerPtWteFemale FLOAT,
	FixedFtNumMale INT,
	FixedFtNumFemale INT,
	FixedFtWteMale FLOAT,
	FixedFtWteFemale FLOAT,
	FixedPtNumMale INT,
	FixedPtNumFemale INT,
	FixedPtWteMale FLOAT,
	FixedPtWteFemale FLOAT,
	SpFtNumMale INT,
	SpFtNumFemale INT,
	SpFtWteMale FLOAT,
	SpFtWteFemale FLOAT,
	SpPtNumMale INT,
	SpPtNumFemale INT,
	SpPtWteMale FLOAT,
	SpPtWteFemale FLOAT,
	CbrNumMale INT,
	CbrNumFemale INT,
	CbrWteMale FLOAT,
	CbrWteFemale FLOAT
) ON [PRIMARY]
END
GO

--DROP TABLE [Dataload].[StafflevelSource]

