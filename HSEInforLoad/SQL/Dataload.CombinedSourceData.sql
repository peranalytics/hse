
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[CombinedSourceData]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[CombinedSourceData](
	GradeId NVARCHAR(50),
	GenderId INT,
	AgencyId INT,
	YearId INT,
	MonthId INT,
	EmpTypeId INT,
	PerFtNum INT,
	PerFtWte FLOAT,
	PerPtNum INT,
	PerPtWte FLOAT,
	FixedFtNum INT,
	FixedFtWte FLOAT,
	FixedPtNum INT,
	FixedPtWte FLOAT,
	SpFtNum INT,
	SpFtWte FLOAT,
	SpPtNum INT,
	SpPtWte FLOAT,
	CbrNum INT,
	CbrWte FLOAT,
) ON [PRIMARY]
END
GO

--DROP TABLE [Dataload].[CombinedSourceData]

