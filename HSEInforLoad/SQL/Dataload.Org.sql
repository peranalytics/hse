
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[Org]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[Org](
	HseOrg INT,
	OrgNo INT,
	OrgName NVARCHAR(500),
	ShortName NVARCHAR(500),
	HbNo INT,
	AhbNo INT,
	Addr1 NVARCHAR(255),
	Addr2 NVARCHAR(255),
	Addr3 NVARCHAR(255),
	Addr4 NVARCHAR(255),
	CoNo INT,
	Phone NVARCHAR(50),
	LglPos NVARCHAR(255),
	Fax NVARCHAR(50),
	TOrg INT,
	HistRef NVARCHAR(255),
	AdminAgencyType INT,
	AdminAgencyNo INT,
	Tsu INT,
	HospitalClass INT,
	GtnNo NVARCHAR(255),
	SpeedDial NVARCHAR(255),
	Fteach NVARCHAR(10),
	Ftrain NVARCHAR(10),
	Obsolete NVARCHAR(10),
	HospitalType INT,
	HseRegionNo INT,
	HospitalNet INT,
	Nuts2 INT,
	Nuts3 INT,
	LhoNo INT,
	PillarNo INT,
	CasemixHospNo INT,
	OrgNoSubs INT,
	IsaNo INT,
	IsAcuteHospital NVARCHAR(255),
	ChoNo INT
) ON [PRIMARY]
END
GO

--DROP TABLE [Dataload].[Org]

SELECT * FROM  [Dataload].[Org]

DELETE FROM  [Dataload].[Org]
INSERT INTO [Dataload].[Org]
SELECT * FROM org$