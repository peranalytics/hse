USE HSE_Numbers

DECLARE @Year INT
DECLARE @Month INT

SET @Year = 2018
SET @Month = 10

DECLARE @GenderIdMale INT, @GenderIdFemale INT, @FtEmpTypeId INT, @PtEmpTypeId INT, @FixedFtTypeId INT, @FixedPtTypeId INT, @SpFtTypeId INT, @SpPtTypeId INT, @CbrId INT
SET @GenderIdMale = 1
SET @GenderIdFemale = 2

SET @FtEmpTypeId = 1
SET @PtEmpTypeId = 4
SET @FixedFtTypeId = 9
SET @FixedPtTypeId = 10
SET @SpFtTypeId = 11
SET @SpPtTypeId = 12
SET @CbrId = 8

DELETE FROM [Dataload].[StafflevelSource]
DELETE FROM [Dataload].[CombinedSourceData]
DELETE FROM [Dataload].[FilteredSourceData]

INSERT INTO [Dataload].[StafflevelSource]([AgencyCode],[GradeCode],[PerFtNumMale],[PerFtNumFemale],[PerFtWteMale],[PerFtWteFemale],[PerPtNumMale],[PerPtNumFemale],[PerPtWteMale],[PerPtWteFemale],[FixedFtNumMale],[FixedFtNumFemale],[FixedFtWteMale],[FixedFtWteFemale],[FixedPtNumMale],[FixedPtNumFemale],[FixedPtWteMale],[FixedPtWteFemale],[SpFtNumMale],[SpFtNumFemale],[SpFtWteMale],[SpFtWteFemale],[SpPtNumMale],[SpPtNumFemale],[SpPtWteMale],[SpPtWteFemale],[CbrNumMale],[CbrNumFemale],[CbrWteMale],[CbrWteFemale])
SELECT [AgencyCode],[GradeCode],[PerFtNumMale],[PerFtNumFemale],[PerFtWteMale],[PerFtWteFemale],[PerPtNumMale],[PerPtNumFemale],[PerPtWteMale],[PerPtWteFemale],[FixedFtNumMale],[FixedFtNumFemale],[FixedFtWteMale],[FixedFtWteFemale],[FixedPtNumMale],[FixedPtNumFemale],[FixedPtWteMale],[FixedPtWteFemale],[SpFtNumMale],[SpFtNumFemale],[SpFtWteMale],[SpFtWteFemale],[SpPtNumMale],[SpPtNumFemale],[SpPtWteMale],[SpPtWteFemale],[CbrNumMale],[CbrNumFemale],[CbrWteMale],[CbrWteFemale] FROM [Staging].[StafflevelSource]


select * from[Staging].[StafflevelSource] where [GradeCode] = '0004'

UPDATE [Dataload].[StafflevelSource]
SET [GradeCode] = RIGHT('0000'+ISNULL([GradeCode],''),4)
WHERE Len([GradeCode]) < 4

--0_d_3 DEL CWS Records
DELETE FROM  [Dataload].[StafflevelSource] WHERE [AgencyCode] IN (SELECT OrgNo FROM Dataload.Org WHERE TSu = 46)

--0_d_5 Get Grade Records
--SELECT * FROM [Dataload].[StafflevelSource] WHERE GradeCode IN (SELECT GrNo FROM Dataload.Gr WHERE GrFamilyNo = 71)

INSERT INTO [Dataload].[CombinedSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[PerFtNum],[PerFtWte],[PerPtNum],[PerPtWte],[FixedFtNum],[FixedFtWte],[FixedPtNum],[FixedPtWte],[SpFtNum],[SpFtWte],[SpPtNum],[SpPtWte],[CbrNum],[CbrWte])
SELECT GradeCode, @GenderIdMale, AgencyCode, YearId, MonthId, EmpTypeId, PerFtNumMale, PerFtWteMale, PerPtNumMale, PerPtWteMale, FixedFtNumMale, FixedFtWteMale, FixedPtNumMale, FixedPtWteMale, SpFtNumMale, SpFtWteMale, SpPtNumMale, SpPtWteMale, CbrNumMale, CbrWteMale FROM [Dataload].[StafflevelSource]

INSERT INTO [Dataload].[CombinedSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[PerFtNum],[PerFtWte],[PerPtNum],[PerPtWte],[FixedFtNum],[FixedFtWte],[FixedPtNum],[FixedPtWte],[SpFtNum],[SpFtWte],[SpPtNum],[SpPtWte],[CbrNum],[CbrWte])
SELECT GradeCode, @GenderIdFemale, AgencyCode, YearId, MonthId, EmpTypeId, PerFtNumFemale, PerFtWteFemale, PerPtNumFemale, PerPtWteFemale, FixedFtNumFemale, FixedFtWteFemale, FixedPtNumFemale, FixedPtWteFemale, SpFtNumFemale, SpFtWteFemale, SpPtNumFemale, SpPtWteFemale, CbrNumFemale, CbrWteFemale FROM [Dataload].[StafflevelSource]

UPDATE [Dataload].[CombinedSourceData]
SET YearId = @Year,
MonthId = @Month

--01_qry_a_UD_PER_FT_Data(1)

--01_qry_b_MT_All_Data_Result_PER_FT_Data
INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @FtEmpTypeId, PerFtNum AS Headcount, PerFtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @PtEmpTypeId, PerPtNum AS Headcount, PerPtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @FixedFtTypeId, FixedFtNum AS Headcount, FixedFtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @FixedPtTypeId, FixedPtNum AS Headcount, FixedPtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @SpFtTypeId, SpFtNum AS Headcount, SpFtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @SpPtTypeId, SpPtNum AS Headcount, SpPtWte AS Wte FROM [Dataload].[CombinedSourceData]

INSERT INTO [Dataload].[FilteredSourceData]([GradeId],[GenderId],[AgencyId],[YearId],[MonthId],[EmpTypeId],[HeadCount],[Wte])
SELECT GradeId, GenderId, AgencyId, YearId, MonthId, @CbrId, CbrNum AS Headcount, CbrWte AS Wte FROM [Dataload].[CombinedSourceData]

SELECT EmpTypeId, COUNT(GradeId) FROM [Dataload].[FilteredSourceData]
GROUP BY EmpTypeId

select * from [Dataload].[FilteredSourceData]
DELETE FROM [Dataload].[FilteredSourceData] WHERE(HeadCount = 0 OR HeadCount IS NULL) AND (Wte = 0 OR Wte IS NULL)

SELECT GradeId, GenderId, AgencyId, YearId, MonthId, EmpTypeId, SUM(HeadCount) AS HeadCount, SUM(Wte) As WTE FROM [Dataload].[FilteredSourceData] 
GROUP BY GradeId, GenderId, AgencyId, YearId, MonthId, EmpTypeId

select * from unit
SELECT RIGHT('84000000'+ISNULL(Orgno,''),8), * FROM Dataload.Org WHERE  RIGHT('84000000'+ISNULL(Orgno,''),8)  NOT IN (SELECT Mem_Id FROM UNit)

SELECT * FROM [Dataload].[FilteredSourceData] WHERE CAST(AgencyId AS VARCHAR(40)) NOT IN (SELECT Mem_Name FROM UNIT)
SELECT DISTINCT GradeId FROM [Dataload].[FilteredSourceData] WHERE CAST(GradeId AS VARCHAR(40)) NOT IN (SELECT RIGHT('0000'+ISNULL(MEM_NAME,''),4) FROM Line)

select * from Dataload.gr where GrNo = '4'
select * from line where MEM_DESC = 'Director General (HSE)'
select * from [Dataload].[FilteredSourceData] where GradeId = '0004'

select * from line where mem_name = '2636'


select * from Dataload.Org order by OrgName
select * from unit order by MEM_desc

SELECT * FROM [Dataload].[FilteredSourceData]
WHERE AgencyId NOT IN (

SELECT * FROM  [Dataload].[FilteredSourceData])

select * from [Dataload].[FilteredSourceData]
SELECT  *FROM  [Dataload].[CombinedSourceData] where GradeId IS NULL

/*
select * from [dbo].[Copy of All_Result_Data]
except 
select * from [Dataload].[FilteredSourceData]


select * from [dbo].[Copy of All_Result_Data] where grade_id = '0041' and agency_id = 904 and emp_type_id = 1 and gender_id = 1
select * from [Dataload].[FilteredSourceData] where GradeId = '0041' and AgencyId = 904 and EmpTypeId = 1 and GenderId = 1*/

select distinct MEM_PID From line

select * from line where mem_id in (
select distinct MEM_PID From line)

select * from line where MEM_PID = 0