
USE HSE_Numbers
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dataload].[Gr]') AND type in (N'U'))
BEGIN
CREATE TABLE [Dataload].[Gr](
	[GrNo] NVARCHAR(50) NULL,
	[GrName] NVARCHAR(MAX) NULL,
	[GrCat] VARCHAR(50) NULL,
	[PayBs] VARCHAR(50) NULL,
	[Obsolete] VARCHAR(50) NULL,
	[DateCreated] VARCHAR(50) NULL,
	[DateObsolete] VARCHAR(50) NULL,
	[Consolidated] VARCHAR(50) NULL,
	[GrNoPrev] VARCHAR(50) NULL,
	[StaffCensus] VARCHAR(50) NULL,
	[GrNoSubs] VARCHAR(50) NULL,
	[GrFamilyNo] [int] NULL,
	[StaffGroupNo] [int] NULL
) ON [PRIMARY] 

END
GO

--DROP TABLE [Dataload].[Gr]

SELECT * FROM  [Dataload].[Gr]

